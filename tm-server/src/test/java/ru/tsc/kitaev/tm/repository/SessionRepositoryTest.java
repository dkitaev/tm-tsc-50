package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;
import ru.tsc.kitaev.tm.repository.dto.SessionDTORepository;
import ru.tsc.kitaev.tm.repository.dto.UserDTORepository;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class SessionRepositoryTest {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    private final ISessionDTORepository sessionRepository;

    @NotNull
    private final IUserDTORepository userRepository;

    @NotNull
    private final SessionDTO session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    public SessionRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDTORepository(entityManager);
        user = new UserDTO();
        user.setLogin("test");
        userId = user.getId();
        @NotNull final String password = "test";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        entityManager.getTransaction().begin();
        userRepository.add(user);
        entityManager.getTransaction().commit();
        sessionRepository = new SessionDTORepository(entityManager);
        session = new SessionDTO();
        sessionId = session.getId();
        session.setUserId(userId);
        session.setTimestamp(System.currentTimeMillis());
    }

    @Before
    public void before() {
    }

    @Test
    @Category(UnitCategory.class)
    public void openCloseTest() {
        entityManager.getTransaction().begin();
        sessionRepository.add(session);
        entityManager.getTransaction().commit();
        @NotNull final SessionDTO tempSession = sessionRepository.findById(sessionId);
        Assert.assertEquals(session.getId(), tempSession.getId());
        Assert.assertEquals(session.getUserId(), tempSession.getUserId());
        Assert.assertEquals(session.getSignature(), tempSession.getSignature());
        entityManager.getTransaction().begin();
        sessionRepository.removeById(session.getId());
        entityManager.getTransaction().commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        entityManager.getTransaction().begin();
        sessionRepository.clear();
        userRepository.remove(user);
        entityManager.getTransaction().commit();
    }

}
