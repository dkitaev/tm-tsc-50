package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().clear(session.getUserId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().findAll(session.getUserId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findAllTaskSorted(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().findAll(session.getUserId(), sort);
    }

    @WebMethod
    @Override
    @Nullable
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().findById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    @NotNull
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().removeById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    @NotNull
    public TaskDTO createTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().create(session.getUserId(), name, description);
    }

    @WebMethod
    @Override
    @NotNull
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().findByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().removeByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @Override
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @Override
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().startById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().startByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().startByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().finishById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().finishByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().changeStatusById(session.getUserId(), id, status);
    }

    @WebMethod
    @Override
    public void changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    @Override
    public void changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getTaskDTOService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") final int index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().existsByIndex(session.getUserId(), index);
    }

}
