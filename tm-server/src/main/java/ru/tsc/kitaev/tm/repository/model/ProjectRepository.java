package ru.tsc.kitaev.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.api.repository.model.IProjectRepository;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    protected final EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @Override
    public void update(@NotNull final Project project) {
        entityManager.merge(project);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Project").executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM Project WHERE user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return entityManager
                .createQuery("FROM Project", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Project> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @NotNull
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId AND id = :id", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    @NotNull
    public Project findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findById(userId, id));
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findByIndex(userId, index));
    }

    @Override
    @NotNull
    public Integer getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Project p WHERE user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    @NotNull
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId AND name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        entityManager.remove(findByName(userId, name));
    }

}
