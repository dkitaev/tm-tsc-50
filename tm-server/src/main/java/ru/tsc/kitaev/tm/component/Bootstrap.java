package ru.tsc.kitaev.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.service.*;
import ru.tsc.kitaev.tm.api.service.dto.*;
import ru.tsc.kitaev.tm.api.service.model.*;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.service.*;
import ru.tsc.kitaev.tm.service.dto.*;
import ru.tsc.kitaev.tm.service.model.*;
import ru.tsc.kitaev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IProjectDTOService projectDTOService = new ProjectDTOService(connectionService, loggerService);

    @NotNull
    private final IProjectTaskDTOService projectTaskDTOService = new ProjectTaskDTOService(connectionService, loggerService);

    @NotNull
    private final ISessionDTOService sessionDTOService = new SessionDTOService(connectionService, loggerService, this);

    @NotNull
    private final ITaskDTOService taskDTOService = new TaskDTOService(connectionService, loggerService);

    @NotNull
    private final IUserDTOService userDTOService = new UserDTOService(connectionService, loggerService, propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService, loggerService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService, loggerService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, loggerService, this);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService, loggerService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, loggerService, propertyService);

    @NotNull
    private final Backup backup = new Backup(domainService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void start(@Nullable final String[] args) {
        try {
            BasicConfigurator.configure();
            initPID();
            loggerService.initJmsLogger();
            initEndpoint();
            //backup.init();
            //initUsers();
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.exit(1);
            }
    }

    private void initEndpoint() {
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(projectTaskEndpoint);
        registry(userEndpoint);
        registry(adminEndpoint);
        registry(adminUserEndpoint);
        registry(sessionEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final Integer port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initUsers() {
            userDTOService.create("test", "test");
            userDTOService.create("admin", "admin", Role.ADMIN);
    }

}
