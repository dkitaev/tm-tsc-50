package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.api.service.dto.*;
import ru.tsc.kitaev.tm.api.service.model.*;

public interface IServiceLocator {

    @NotNull
    ITaskDTOService getTaskDTOService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectDTOService getProjectDTOService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskDTOService getProjectTaskDTOService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserDTOService getUserDTOService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDTOService getSessionDTOService();

    @NotNull
    ISessionService getSessionService();

}
